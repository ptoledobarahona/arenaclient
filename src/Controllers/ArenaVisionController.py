import sys
sys.path.append('..')
from Models.ArenaVision import ArenaVision


class ArenaVisionController:
    def __init__(self):
        self.arena_vision = ArenaVision()

    def get_formatted_event_list(self):
        return self.arena_vision.get_formatted_event_list()

    def get_channels(self,event, player='acestream'):
        return self.arena_vision.get_channels(event, player)

    def get_link(self,channel):
        return self.arena_vision.get_link(channel)

    def play(self, link):
        self.arena_vision.play(link)
