import sys
sys.path.append("..")
from Models.Config import Config

class ConfigController:
    def __init__(self):
        self.config = Config()

    def accept(self, values):
        if self.config.save_config_data(values):
            return True

    def get_acestream_ex(self):
        return self.config.get_acestream_ex()

    def get_sopcast_ex(self):
        return self.config.get_sopcast_ex()