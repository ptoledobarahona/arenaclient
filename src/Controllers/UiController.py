import sys

sys.path.append("..")
from Views.ConfigDialog import ConfigDialog
from Views.FileDialog import FileDialog
from Views.MessageBox import MessageBox


class UiController:
    def __init__(self):
        self.config_dialog = ConfigDialog ()
        self.file_dialog = FileDialog

    def get_config_dialog(self):
        return self.config_dialog

    @staticmethod
    def get_file_dialog():
        file_dialog = FileDialog()
        return file_dialog

    @staticmethod
    def get_message_box(title, msg):
        msg_box = MessageBox(title, msg)
        msg_box.show_messsage()