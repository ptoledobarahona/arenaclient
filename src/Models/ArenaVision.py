import sys
sys.path.append('..')
from Controllers.ConfigController import ConfigController
import urllib.request, urllib.error, urllib.parse

from bs4 import BeautifulSoup
import re
import subprocess

class ArenaVision:
    def __init__(self):
      self.cached_event_list=self.__get_event_list()

    def __get_event_list(self):
        url=('http://arenavision.in/agenda')
        hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3M;unicode',
        'Accept-Encoding': 'none',
        'Connection': 'keep-alive'}

        req = urllib.request.Request(url,headers=hdr)
        page = urllib.request.urlopen(req)
        soup = BeautifulSoup(page,"html.parser")
        a = soup.findAll('p')
        schedule = a[1]
        schedule = str(schedule).split("<br/>")
        return schedule

    def get_formatted_event_list(self):

        event_list = self.cached_event_list
        channel_pattern = re.compile("([/](AV)[0-9]+)+")
        b_pattern = re.compile("(<p>)")
        bc_pattern = re.compile("(</p>)")
        output_list = []
        for event in event_list:
          event = re.sub(b_pattern, r'', event)
          event = re.sub(bc_pattern, r'', event)
          event = re.sub(channel_pattern, r'', event)
          output_list.append(event)
        return output_list

    def get_channels(self,event, player='acestream'):
        self.__get_event_list()
        channel_list=None
        output_list=[]
        event_list = self.cached_event_list
        for item in event_list:
            if event in item:
                channel_list = self.__channel_extractor(item)
        if player=='acestream':
            for  channel in channel_list:
                if int(channel) < 21:
                    output_list.append(channel)
        else:
            for channel in channel_list:
                if int(channel) >= 21:
                    output_list.append(channel)
        return output_list

    @staticmethod
    def __channel_extractor(event):
        output_list = []
        channels = str(event).split("/AV")
        del(channels[0])
        for channel in channels:
            if channel is not None:
                output_list.append(channel)
        return output_list

    @staticmethod
    def get_link(channel):
        ace_pattern = re.compile('(acestream://)[a-z0-9A-Z]+')
        sop_pattern = re.compile('(sop://broker.sopcast.com:)[0-9/]+')
        url = "http://arenavision.in/AV" + str(channel)
        hdr = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Accept-Encoding': 'none',
            'Connection': 'keep-alive'}

        req = urllib.request.Request(url, headers=hdr)
        page = urllib.request.urlopen(req)
        soup = BeautifulSoup(page, 'html.parser')
        links = soup.findAll('a')

        for link in links:
            if re.match(ace_pattern,link.get('href')) or \
               re.match(sop_pattern,link.get('href')):
                return link.get('href')

    @staticmethod
    def play(link):
        config_controller =ConfigController()
        if 'acestream' in link:
            ace=config_controller.get_acestream_ex()
            subprocess.call(str(ace)+' '+str(link))
        elif 'sop' in link:
            sop=config_controller.get_sopcast_ex()
            subprocess.call(str(sop)+' '+str(link))

