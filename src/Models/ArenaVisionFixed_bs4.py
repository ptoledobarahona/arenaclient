from bs4 import BeautifulSoup
import requests
import re
class ArenaVision():
    def __init__(self):
        self.url = 'http://arenavision.in/schedule'
        self.channel_url = 'http://arenavision.in/av'

    def get_event_list(self):

        req = requests.get(self.url)
        status_code = req.status_code
        html_text = req.text.encode('utf-8')
        #IF RESPONSE IS 200
        if status_code == 200:
            html = BeautifulSoup(html_text,'lxml')
            rows = html.find_all('tr')
            event_list = []
            for row in rows:
                event = {}
                try:
                    event['TIME'] = row.find('td',attrs={'style': 'width: 182px'}).getText().encode('utf-8')
                    event['DATE'] = row.find('td',attrs={'style':'width: 190px'}).getText().encode('utf-8')
                    event['SPORT'] = row.find('td',attrs={'style':'width: 188px'}).getText().encode('utf-8')
                    event['TOURNAMENT'] = row.find('td',attrs={'style':'width: 283px'}).getText().encode('utf-8')
                    event['MATCH'] = row.find('td',attrs={'style':'width: 685px'}).getText().encode('utf-8')
                    raw_channels = row.find('td',attrs={'style':'width: 317px'}).getText().encode('utf-8')
                    channels = []
                    for channel in raw_channels.split('[')[0].split('-'):
                        channels.append(channel)
                    event['CHANNEL'] = channels
                    event_list.append(event)

                except Exception as e:
                    pass
                    #print ('<WRN>: '+ str(e))

        else: print ('<ERR>: PAGE NOT AVALIABLE RIGHT NOW')
        return event_list

    def get_stream_link(self, channel):

        url = str(self.channel_url)+str(channel)
        req = requests.get(url)
        status_code = req.status_code
        html_text = req.text.encode('utf-8')
        if status_code == 200:
            html = BeautifulSoup(html_text,'lxml')
            link_tag = html.find_all('a',href=re.compile('acestream://'))
            for link in link_tag:
                return link['href']

        else: print ('<ERR>: PAGE NOT AVALIABLE RIGHT NOW')

    def get_event_by_key(self, k):
        output_list = []
        try:
            for item in self.get_event_list():
                for key, value in item.iteritems():
                    if k in value:
                        output_list.append(item)

        except Exception as e:
            print '<WRN>: String not found ' +str(e)

        if len(output_list) != 0: return output_list
        
def test(search_field =''):
    arena = ArenaVision()
    try:
        for item in arena.get_event_by_key(search_field):
            print item['DATE'] +' '+ item['TIME']+' '+item['SPORT']+' '+item['TOURNAMENT']+' '+item['MATCH']+' '+str(item['CHANNEL'])
    except TypeError as e:
        print "<WRN> " + str(e)

if __name__ == "__main__":

    if len(sys.argv) > 1:
        arg = sys.argv[1].upper()
        test(arg)
    else:
        test()



