import os
import json

class Config:
    def __init__(self):
        self.json_path = '..\\Views\\resources\\config.json'

    def save_config_data(self, values):
        with open(self.json_path,'w') as data_file:
            try:
                json.dump(values, data_file)
                return True
            except IOError as e:
                print(e)
                return false

    def __read_config_data(self):
        config_path = self.json_path
        with open(config_path) as json_data_file:
            if json_data_file:
                config_parsed_json = (json.load(json_data_file))
                return config_parsed_json
            else:
                return False

    def get_acestream_ex(self):
        config_data = self.__read_config_data()
        return config_data['players_path']['acestream']
    def get_sopcast_ex(self):
        config_data = self.__read_config_data()
        return config_data['players_path']['sopcast']