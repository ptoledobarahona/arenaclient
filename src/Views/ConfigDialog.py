# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'config.ui'
#
# Created: Sat Apr 23 01:00:14 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

from Controllers.ConfigController import ConfigController


class ConfigDialog(QtGui.QDialog):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config_controller = ConfigController()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(320, 240)
        self.setMaximumSize(320,240)
        self.setMinimumSize(320,240)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(10, 200, 301, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.ace_line = QtGui.QLineEdit(Dialog)
        self.ace_line.setGeometry(QtCore.QRect(20, 50, 211, 20))
        self.ace_line.setObjectName("ace_line")
        self.ace_line.setEnabled(False)
        self.sop_line = QtGui.QLineEdit(Dialog)
        self.sop_line.setGeometry(QtCore.QRect(20, 110, 211, 20))
        self.sop_line.setObjectName("sop_line")
        self.sop_line.setEnabled(False)
        self.ace_label = QtGui.QLabel(Dialog)
        self.ace_label.setGeometry(QtCore.QRect(20, 30, 91, 16))
        self.ace_label.setObjectName("ace_label")
        self.sop_label = QtGui.QLabel(Dialog)
        self.sop_label.setGeometry(QtCore.QRect(20, 90, 91, 16))
        self.sop_label.setObjectName("sop_label")
        self.ace_button = QtGui.QPushButton(Dialog)
        self.ace_button.setGeometry(QtCore.QRect(250, 50, 31, 23))
        self.ace_button.setObjectName("ace_button")
        self.sop_button = QtGui.QPushButton(Dialog)
        self.sop_button.setGeometry(QtCore.QRect(250, 110, 31, 23))
        self.sop_button.setObjectName("sop_button")

        #SIGNALS
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.ace_button.clicked.connect(self.ace_path)
        self.sop_button.clicked.connect(self.sop_path)
        self.set_active_values()
        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Config", None, QtGui.QApplication.UnicodeUTF8))
        Dialog.setWindowIcon(QtGui.QIcon('resources\\config.png'))
        self.ace_label.setText(QtGui.QApplication.translate("Dialog", "AceStream path:", None, QtGui.QApplication.UnicodeUTF8))
        self.sop_label.setText(QtGui.QApplication.translate("Dialog", "SopCast path: ", None, QtGui.QApplication.UnicodeUTF8))
        self.ace_button.setText(QtGui.QApplication.translate("Dialog", "...", None, QtGui.QApplication.UnicodeUTF8))
        self.sop_button.setText(QtGui.QApplication.translate("Dialog", "...", None, QtGui.QApplication.UnicodeUTF8))

    def accept(self):
        acestream = self.ace_line.text()
        sopcast = self.sop_line.text()

        values = {
            'players_path':{
                  'acestream': acestream,
                  'sopcast': sopcast}
                 }
        accept = self.config_controller.accept(values)
        if accept:
            from Controllers.UiController import UiController
            ui_controller = UiController()
            ui_controller.get_message_box('Exito', 'Configuración guardada')
            self.reject()

    def search_file(self,player):
        from Controllers.UiController import UiController
        file_dialog = UiController.get_file_dialog()
        file_param = ('/home','EXE(*.exe)')
        file_path = file_dialog.show_open_dialog(file_param)
        if player =='ace':
            self.ace_line.setText(file_path)
        else:
            self.sop_line.setText(file_path)


    def ace_path(self):
        self.search_file('ace')
    def sop_path(self):
        self.search_file('sop')

    def set_active_values(self):
        config_controller= ConfigController()
        self.ace_line.setText(config_controller.get_acestream_ex())
        self.sop_line.setText(config_controller.get_sopcast_ex())