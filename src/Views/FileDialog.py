from PySide import QtGui
class FileDialog(object):
    def __init__(self):
        pass
    def show_open_dialog(self, file_param, validate=False):
        assert len(file_param) == 2
        filename, exten = QtGui.QFileDialog.getOpenFileName(None, 'open file',
                                                            file_param[0], file_param[1])
        if validate:
            if self.validator.validate_dxf(filename) == 0:
                Global.LOADED_FILE = filename
                return True
            elif self.validator.validate_dxf(filename) == 1:
                Global.FILE_NOT_VALID = True
                Global.RELOAD_CONTROL = False
            elif self.validator.validate_dxf(filename) == 2:
                Global.FILE_NOT_VALID = False
                Global.RELOAD_CONTROL = True
        return filename

    @staticmethod
    def show_save_dialog(file_param):
        fname, exten = QtGui.QFileDialog.getSaveFileName(None, 'save file as: ',
                                                         file_param[0], file_param[1])
        if fname:
            Global.SAVED_FILE = fname
            return exten

    @staticmethod
    def show_folder_dialog():
        fname = QtGui.QFileDialog.getExistingDirectory()
        return fname
