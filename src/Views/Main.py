
import sys
sys.path.append("..")

from PySide import QtCore, QtGui
from Controllers.ArenaVisionController import ArenaVisionController
from Controllers.UiController import UiController


class Ui_ArenaClient(QtGui.QMainWindow):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.arenavision_controller = ArenaVisionController()
        self.ui_controller = UiController()

    def setupUi(self, ArenaClient):
        ArenaClient.setObjectName("ArenaClient")
        ArenaClient.resize(1000, 460)
        ArenaClient.setMinimumSize(QtCore.QSize(1000, 460))
        ArenaClient.setMaximumSize(QtCore.QSize(1000, 460))
        self.centralwidget = QtGui.QWidget(ArenaClient)
        self.centralwidget.setObjectName("centralwidget")

        #Frame
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(0, 0, 1000, 600))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName("frame")

        #Line
        self.search_field = QtGui.QLineEdit(self.frame)
        self.search_field.setGeometry(QtCore.QRect(580, 430, 350, 20))
        self.search_field.setObjectName("search_field")
        #Find button
        self.find_pushButton = QtGui.QPushButton(self.frame)
        self.find_pushButton.setGeometry(QtCore.QRect(940, 428, 50, 25))
        self.find_pushButton.setObjectName("find_button")
        self.find_icon = QtGui.QIcon('..\\Views\\resources\\find.png')
        self.find_pushButton.setIcon(self.find_icon)
        #channels combo box
        self.channel_combo_box = QtGui.QComboBox(self.frame)
        self.channel_combo_box.setObjectName('Channel_combo')
        self.channel_combo_box.setGeometry(QtCore.QRect(730,225,100,25))
        #channel_combo_box_label
        self.channel_combo_box_label = QtGui.QLabel(self.frame)
        self.channel_combo_box_label.setGeometry(QtCore.QRect(730,200,100,25))
        self.channel_combo_box_label.setText('Channel')
        self.channel_combo_box_label.hide()

        if self.channel_combo_box.count()==0: self.channel_combo_box.hide()
        self.scrollArea = QtGui.QScrollArea(self.frame)
        self.scrollArea.setGeometry(QtCore.QRect(10, 10, 550, 441))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 700, 439))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.listWidget = QtGui.QListWidget(self.scrollAreaWidgetContents)
        self.listWidget.setGeometry(QtCore.QRect(0, 0, 550, 441))
        self.listWidget.setObjectName("listWidget")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.calendarWidget = QtGui.QCalendarWidget(self.frame)
        self.calendarWidget.setGeometry(QtCore.QRect(580, 10, 400, 161))
        self.calendarWidget.setObjectName("calendarWidget")
        self.line = QtGui.QFrame(self.frame)
        self.line.setGeometry(QtCore.QRect(580, 180, 410, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.sopcast_radio = QtGui.QRadioButton(self.frame)
        self.sopcast_radio.setGeometry(QtCore.QRect(580, 200, 82, 17))
        self.sopcast_radio.setObjectName("sopcast_radio")
        self.acestream_radio = QtGui.QRadioButton(self.frame)
        self.acestream_radio.setGeometry(QtCore.QRect(580, 230, 82, 17))
        self.acestream_radio.setObjectName("acestream_radio")
        self.acestream_radio.setChecked(True)
        #play_button
        self.play_button = QtGui.QPushButton(self.frame)
        self.play_button.setGeometry(QtCore.QRect(890, 200, 101, 51))
        self.play_button.setObjectName("play_button")
        self.play_button_icon = QtGui.QIcon('..\\Views\\resources\\play.png')
        self.play_button.setIcon(self.play_button_icon)
        #image
        self.image_label = QtGui.QLabel(self.frame)
        self.image_pixmap = QtGui.QPixmap('..\\Views\\resources\\sports.png')
        self.image_label.setPixmap(self.image_pixmap)
        self.image_label.setGeometry(QtCore.QRect(800,270,150,150))
        self.settings_button = QtGui.QPushButton(self.frame)
        self.settings_button.setGeometry(QtCore.QRect(580, 400, 75, 23))
        self.settings_button.setObjectName("settings_button")
        self.settings_button_icon = QtGui.QIcon('..\\Views\\resources\\config.png')
        self.settings_button.setIcon(self.settings_button_icon)
        self.refresh_button = QtGui.QPushButton(self.frame)
        self.refresh_button.setGeometry(QtCore.QRect(580, 375, 75, 23))
        self.refresh_button.setObjectName("refresh_button")
        self.refresh_button_icon = QtGui.QIcon('..\\Views\\resources\\refresh.png')
        self.refresh_button.setIcon(self.refresh_button_icon)
        ArenaClient.setCentralWidget(self.centralwidget)
        ArenaClient.setWindowIcon(QtGui.QIcon('..\\Views\\resources\\sports.png'))
        self.retranslateUi(ArenaClient)
        QtCore.QMetaObject.connectSlotsByName(ArenaClient)
        self.populate_event_list()

        #Signals
        self.listWidget.clicked.connect(self._event)
        self.refresh_button.clicked.connect(self.refresh)
        self.calendarWidget.clicked.connect(self.calendar)
        self.acestream_radio.clicked.connect(self.radio_event)
        self.sopcast_radio.clicked.connect(self.radio_event)
        self.play_button.clicked.connect(self.player)
        self.find_pushButton.clicked.connect(self.find)
        self.settings_button.clicked.connect(self.config)


    def retranslateUi(self, ArenaClient):
        ArenaClient.setWindowTitle(QtGui.QApplication.translate("ArenaClient", "ArenaClient", None, QtGui.QApplication.UnicodeUTF8))
        self.sopcast_radio.setText(QtGui.QApplication.translate("ArenaClient", "Sopcast", None, QtGui.QApplication.UnicodeUTF8))
        self.acestream_radio.setText(QtGui.QApplication.translate("ArenaClient", "Acestream", None, QtGui.QApplication.UnicodeUTF8))
        self.play_button.setText(QtGui.QApplication.translate("ArenaClient", "Play!", None, QtGui.QApplication.UnicodeUTF8))
        self.settings_button.setText(QtGui.QApplication.translate("ArenaClient", "Settings", None, QtGui.QApplication.UnicodeUTF8))
        self.refresh_button.setText(QtGui.QApplication.translate("ArenaClient", "Refresh", None, QtGui.QApplication.UnicodeUTF8))

    #click on event
    def _event(self):
        s_event=self.listWidget.currentItem().text()
        ace = self.arenavision_controller.get_channels(s_event,player='acestream')
        sop = self.arenavision_controller.get_channels(s_event,player='sopcast')

        if self.acestream_radio.isChecked() and len(ace)!=0:
            self.channel_combo_box.clear()
            for channel in ace:
                self.channel_combo_box.addItem(channel)
            self.channel_combo_box.show()
            self.channel_combo_box_label.show()

        elif self.sopcast_radio.isChecked() and len(sop)!=0:
            self.channel_combo_box.clear()
            for channel in sop:
                self.channel_combo_box.addItem(channel)
            self.channel_combo_box.show()
            self.channel_combo_box_label.show()

    def radio_event(self):
        if self.listWidget.currentItem() is not None:
            self.channel_combo_box.clear()
            self._event()


    def player(self):
        if self.channel_combo_box.currentText() !='':
            channel = self.channel_combo_box.currentText()
            link = self.arenavision_controller.get_link(channel)
            self.arenavision_controller.play(link)


    def config(self):
        dialog = self.ui_controller.get_config_dialog()
        dialog.setupUi(dialog)
        dialog.exec_()

    def find(self, date=False, d_pattern=None):
        result = []
        if not date:
            text = self.search_field.text()
        else:
            text = d_pattern

        for item in self.populate_event_list():
            if text.upper() in item:
                result.append(item)

        if len(result)!=0:
            self.populate_event_list(search = True, filter_list=result)
        else:
            item = 'NO SE HA ENCONTRADO NINGUN EVENTO'
            self.listWidget.clear()
            self.listWidget.addItem(item)


    #shows avaliable events for the selected date
    def calendar(self):
        date = self.calendarWidget.selectedDate()
        pattern = date.toString('d/MM/yy')
        self.find(date=True, d_pattern=pattern)

    #Refresh event list
    def refresh(self):
        self.listWidget.clear()
        self.populate_event_list()

    #Populate event list
    def populate_event_list(self,search=False, filter_list=None):
        self.listWidget.setFont(self.__list_font())
        event_list = []
        if not search:
            event_list = self.arenavision_controller.get_formatted_event_list()
            for event in event_list:
                self.listWidget.addItem(event)
            return event_list
        else:
            if filter_list is not None:
                self.listWidget.clear()
                for event in filter_list:
                    self.listWidget.addItem(event)
                return event_list



    @staticmethod
    def __list_font():
        font = QtGui.QFont()
        font.setFamily('sans')
        font.setPixelSize(11.5)
        return font

class ControlWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(ControlWindow, self).__init__(parent)
        self.ui = Ui_ArenaClient()
        self.ui.setupUi(self)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = ControlWindow()
    window.show()
    sys.exit(app.exec_())
