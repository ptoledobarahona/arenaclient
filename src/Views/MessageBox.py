
from PySide import QtGui


class MessageBox:
    def __init__(self, title, message):
        self.title = title
        self.message = message

    def show_messsage(self):
        void_message_box = QtGui.QMessageBox()
        void_message_box.setWindowIcon(QtGui.QIcon('resource/msg.png'))

        QtGui.QMessageBox.question(void_message_box, self.title, self.message)

