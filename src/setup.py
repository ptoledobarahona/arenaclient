from distutils.core import setup
import py2exe

setup(
    name='ArenaClient',
    version='1.0',
    packages=['Controllers','Models','Views','Views.resources'],
    url='',
    license='',
    author='Pablo Toledo',
    author_email='ptoledobarahona@gmail.com',
    description='',
    windows=[
                {
                    'script':'Views\\Main.py',
                    "icon_resources":[(1,".\\favicon.ico")]
                }
            ]
    )
